# Teste - Estágio SCC4

### Tecnologias

    • Java 8
    • Maven
    • Springboot
    • REST
    
### Parte 1

Foram implantados todos os métodos solicitados, respeitando o formato da requisição e o da resposta.

### Sistema monetário

Foi implementado a rota /consultarSaque, que possui a query valor.

Exemplo de requisição:

    GET http://localhost:8080/consultarSaque?valor=8
    
Resposta:

    Saque R$8: 1 nota de R$3 e 1 nota de R$5
