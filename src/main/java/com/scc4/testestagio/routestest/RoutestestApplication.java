package com.scc4.testestagio.routestest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RoutestestApplication {

	public static void main(String[] args) {
		SpringApplication.run(RoutestestApplication.class, args);
	}

}
