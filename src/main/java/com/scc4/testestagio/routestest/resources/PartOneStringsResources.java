package com.scc4.testestagio.routestest.resources;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.scc4.testestagio.routestest.utils.StringUtils;

@RestController
public class PartOneStringsResources {

	
	@GetMapping("/tamanho")
	public ResponseEntity<String> lenghtWords(@RequestParam(value = "palavra") String word) {
		
		String message = "tamanho="+word.length();
		
		return ResponseEntity.ok().body(message);
	}
	
	@GetMapping("/maiusculas")
	public ResponseEntity<String> upperCaseWords(@RequestParam(value = "palavra") String words) {
		String upperCaseWord = words.toUpperCase();
		
		return ResponseEntity.ok().body(upperCaseWord);
	}
	
	@GetMapping("/vogais")
	public ResponseEntity<String> vowelsWords(@RequestParam(value = "palavra") String words) {
		String vowels = StringUtils.filterVowels(words);
		
		return ResponseEntity.ok().body(vowels);
	}

	@GetMapping("/consoantes")
	public ResponseEntity<String> consonantsWords(@RequestParam(value = "palavra") String words) {
		String consonants = StringUtils.filterConsonants(words);
		
		return ResponseEntity.ok().body(consonants);
	}
	
	@GetMapping("/nomeBibliografico")
	public ResponseEntity<String> bibliographicName(@RequestParam(value = "nome") String name) {
		String bibliographicName = StringUtils.nameForBibliographicName(name);
		
		return ResponseEntity.ok().body(bibliographicName);
	}
	

}
