package com.scc4.testestagio.routestest.resources;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.scc4.testestagio.routestest.utils.MonetaryUtils;

@RestController
public class MonetarySystemResources {
	
	@GetMapping("/consultarSaque")
	public ResponseEntity<String> consultWithdrawMoney(@RequestParam(value = "valor") int value) {
		
		if(value<3||value==4||value==7||value==14) {	
			return ResponseEntity.badRequest().body("Erro, o valor não pode ser sacado com notas de 3 e 5");
		}
		
		int[] bankNotes = MonetaryUtils.consultWithdrawLessBankNotes(value);
		int valueFive = bankNotes[0];
		int valueThree= bankNotes[1];
		
		String message = "Saque R$"+value+":";
		message += MonetaryUtils.messageQuantityBankNotes(valueThree,3);
		
		if(valueThree > 0 && valueFive > 0)
			message +=" e";
		
		message += MonetaryUtils.messageQuantityBankNotes(valueFive,5);
		
		return ResponseEntity.ok().body(message);
	}
	
}
