package com.scc4.testestagio.routestest.resources;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.scc4.testestagio.routestest.utils.NumberUtils;

@RestController
public class PartOneNumbersResources {
	
	@GetMapping("/listaReversa")
	public ResponseEntity<String> reverseList(@RequestParam(value = "lista") String list) {	
		List<Integer> integerList = new ArrayList<Integer>();
		
		try {
			integerList = NumberUtils.integerListOfString(list);
		}catch(Exception e) {
			return ResponseEntity.badRequest().body("Erro, é esperado valores inteiros");
		}
		
		integerList = NumberUtils.reverseIntegerList(integerList);
				
		String message = NumberUtils.messageArrayInteger(integerList);
		
		return ResponseEntity.ok().body(message);
	}

	@GetMapping("/imprimirImpares")
	public ResponseEntity<String> reverseListOfOdd(@RequestParam(value = "lista") String list) {
		List<Integer> integerList = new ArrayList<Integer>();
		
		try {
			integerList = NumberUtils.integerListOfString(list);
		}catch(Exception e) {
			return ResponseEntity.badRequest().body("Erro, é esperado valores inteiros");
		}
		
		integerList = NumberUtils.reverseIntegerList(integerList);
		integerList = NumberUtils.filterOdd(integerList);
		
		String message = NumberUtils.messageArrayInteger(integerList);
		
		return ResponseEntity.ok().body(message);
	}

	@GetMapping("/imprimirPares")
	public ResponseEntity<String> reverseListOfPairs(@RequestParam(value = "lista") String list) {
		List<Integer> integerList = new ArrayList<Integer>();
		
		try {
			integerList = NumberUtils.integerListOfString(list);
		}catch(Exception e) {
			return ResponseEntity.badRequest().body("Erro, é esperado valores inteiros");
		}
		
		integerList = NumberUtils.reverseIntegerList(integerList);
		integerList = NumberUtils.filterpairs(integerList);
		
		String message = NumberUtils.messageArrayInteger(integerList);
		
		return ResponseEntity.ok().body(message);
	}
	
}
