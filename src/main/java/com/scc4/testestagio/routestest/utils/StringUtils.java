package com.scc4.testestagio.routestest.utils;

public class StringUtils {
	
	public static String filterVowels(String words) {
		char[] characterWords = words.toCharArray();
		String vowels = "";
		
		for(char character : characterWords){
			if(isVowel(character)) {
				vowels+=character;
			}
		}
				
		return vowels;
	}
	
	public static String filterConsonants(String words) {
		char[] characterWords = words.toCharArray();
		String consonants = "";
		
		for(char character : characterWords){
			if(isConsonant(character)) {
				consonants+=character;
			}
		}
				
		return consonants;
	}
	
	private static boolean isVowel(char character){
		return (character=='a'
				||character=='e'
				||character=='i'
				||character=='o'
				||character=='u'
				||character=='A'
				||character=='E'
				||character=='I'
				||character=='O'
				||character=='U'
				);
	}
	
	private static boolean isConsonant(char character){
		return (character>=66 && character<=68
				||character>=70 && character<=72
				||character>=74 && character<=78
				||character>=80 && character<=84
				||character>=86 && character<=90
				||character>=98 && character<=100
				||character>=102 && character<=104
				||character>=106 && character<=110
				||character>=112 && character<=116
				||character>=118 && character<=122				
				);
	}
	
	public static String nameForBibliographicName(String name) {
		String[] partsName = name.trim().split(" ");
		int lastNamePosition = partsName.length-1;
		
		String bibliographicName = partsName[lastNamePosition].toUpperCase();
		bibliographicName += ",";
		
		for (int part = 0; part < lastNamePosition; part++) {
			
			String partName = partsName[part].substring(0, 1).toUpperCase();
			partName += partsName[part].substring(1).toLowerCase();
			
			bibliographicName += " "+partName;
		}
		
		return bibliographicName;
	}
}
