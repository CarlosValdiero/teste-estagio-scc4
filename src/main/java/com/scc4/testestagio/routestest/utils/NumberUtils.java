package com.scc4.testestagio.routestest.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class NumberUtils {
	
public static List<Integer> reverseIntegerList(List<Integer>list) {
		List<Integer> listClone = new ArrayList<Integer>(list);
		
		Collections.reverse(listClone);
		
		return listClone;
	}
	
	public static List<Integer> filterOdd(List<Integer> list) {

		list = list.stream().filter(item -> item%2!=0).collect(Collectors.toList());
		
		return list;
	}
	
	public static List<Integer> filterpairs(List<Integer> list) {

		list = list.stream().filter(item -> item%2==0).collect(Collectors.toList());
		
		return list;
	}
	
	public static List<Integer> integerListOfString(String list) {
		String[] items = list.split(",");
		ArrayList<Integer> newList = new ArrayList<>();
		
		for (int positionItem = 0; positionItem < items.length; positionItem++) {
			newList.add(Integer.parseInt(items[positionItem]));
		}
		
		return newList;
	}
	
	public static String messageArrayInteger(List<Integer> list) {
		if(list.isEmpty()) return "{ }";
		
		String message ="{";
		
		for(int number : list) {
			message +=number+",";
		}
		
		message = message.substring (0, message.length() - 1)+"}";
		
		return message;
	}
	
	
	

}
