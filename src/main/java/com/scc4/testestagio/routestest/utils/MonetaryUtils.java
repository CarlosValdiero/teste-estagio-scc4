package com.scc4.testestagio.routestest.utils;

public class MonetaryUtils {

	public static int[] consultWithdrawLessBankNotes(int value) {
		int valueFive, valueThree=0, restDivision;
		
		valueFive = value/5;
		restDivision = value%5;
		
		if(restDivision==0) {
			return new int[]{valueFive, valueThree};
		}
		
		valueThree = restDivision/3;
		restDivision = restDivision%3;
		
		
		while(restDivision!=0 && valueFive>0){
		
		valueFive--;
		restDivision = value-valueFive*5;
		
		valueThree = restDivision/3;
		restDivision = restDivision%3;
		
		}
		
		return new int[]{valueFive, valueThree};
		
	}
	
	public static String messageQuantityBankNotes(int quantity, int type) {
		String message="";
		
		if(quantity != 0) {
			message =" "+quantity;
			if(quantity>1) {
				message += " notas de R$"+type;
			}else {
				message += " nota de R$"+type;
			}	
		}
		
		return message;
	}
}
