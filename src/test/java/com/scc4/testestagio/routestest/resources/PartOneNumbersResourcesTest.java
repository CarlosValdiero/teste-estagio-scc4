package com.scc4.testestagio.routestest.resources;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.apache.catalina.connector.Response;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

@SpringBootTest
public class PartOneNumbersResourcesTest {

	@Autowired
	PartOneNumbersResources numbersResources;
	
	@Test
	public void testReverseList() {
		ResponseEntity<String> response = numbersResources.reverseList("25,35,40,21");
		
		assertEquals(Response.SC_OK,response.getStatusCodeValue());
		assertEquals("{21,40,35,25}",response.getBody());
	}
	
	@Test
	public void testReverseListOfOdd() {
		ResponseEntity<String> response = numbersResources.reverseListOfOdd("25,35,40,21");
		
		assertEquals(Response.SC_OK,response.getStatusCodeValue());
		assertEquals("{21,35,25}",response.getBody());
	}
	
	@Test
	public void testReverseListOfPair() {
		ResponseEntity<String> response = numbersResources.reverseListOfPairs("25,35,40,21");
		
		assertEquals(Response.SC_OK,response.getStatusCodeValue());
		assertEquals("{40}",response.getBody());
	}
	
	@Test
	public void testReverseListFailed() {
		ResponseEntity<String> response = numbersResources.reverseList("25,35,40,2a");
		
		assertEquals(Response.SC_BAD_REQUEST,response.getStatusCodeValue());
		assertEquals("Erro, é esperado valores inteiros",response.getBody());
	}
	
	@Test
	public void testReverseListOfOddFailed() {
		ResponseEntity<String> response = numbersResources.reverseListOfOdd("25,35,40,2a");
		
		assertEquals(Response.SC_BAD_REQUEST,response.getStatusCodeValue());
		assertEquals("Erro, é esperado valores inteiros",response.getBody());
	}
	
	@Test
	public void testReverseListOfPairFailed() {
		ResponseEntity<String> response = numbersResources.reverseListOfPairs("25,35,40,2a");
		
		assertEquals(Response.SC_BAD_REQUEST,response.getStatusCodeValue());
		assertEquals("Erro, é esperado valores inteiros",response.getBody());
	}
}
