package com.scc4.testestagio.routestest.resources;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.apache.catalina.connector.Response;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

@SpringBootTest
public class MonetarySystemResourcesTest {
	
	@Autowired
	MonetarySystemResources monetaryResources;
	
	@Test
	public void testConsultWithdrawMoney() {
		ResponseEntity<String> response = monetaryResources.consultWithdrawMoney(8);
		
		assertEquals(Response.SC_OK,response.getStatusCodeValue());
		assertEquals("Saque R$8: 1 nota de R$3 e 1 nota de R$5",response.getBody());
	}
	
	@Test
	public void testConsultWithdrawMoneyFailed() {
		ResponseEntity<String> response = monetaryResources.consultWithdrawMoney(14);
		
		assertEquals(Response.SC_BAD_REQUEST,response.getStatusCodeValue());
		assertEquals("Erro, o valor não pode ser sacado com notas de 3 e 5",response.getBody());
	}
}
