package com.scc4.testestagio.routestest.resources;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.apache.catalina.connector.Response;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

@SpringBootTest
public class PartOneStringsResourcesTest {
	
	@Autowired
	PartOneStringsResources stringsResources;
	
	@Test
	public void testLenghtWords() {
		ResponseEntity<String> response = stringsResources.lenghtWords("desafio");
		
		assertEquals(Response.SC_OK,response.getStatusCodeValue());
		assertEquals("tamanho=7",response.getBody());
	}
	
	@Test
	public void testUpperCaseWords() {
		ResponseEntity<String> response = stringsResources.upperCaseWords("desafio");
		
		assertEquals(Response.SC_OK,response.getStatusCodeValue());
		assertEquals("DESAFIO",response.getBody());
	}
	
	@Test
	public void testVowelsWords() {
		ResponseEntity<String> response = stringsResources.vowelsWords("desafio");
		
		assertEquals(Response.SC_OK,response.getStatusCodeValue());
		assertEquals("eaio",response.getBody());
	}
	
	@Test
	public void testConsonantsWords() {
		ResponseEntity<String> response = stringsResources.consonantsWords("desafio");
		
		assertEquals(Response.SC_OK,response.getStatusCodeValue());
		assertEquals("dsf",response.getBody());
	}
	
	@Test
	public void testBibliographicName() {
		ResponseEntity<String> response = stringsResources.bibliographicName("paulo pereira junior");
		
		assertEquals(Response.SC_OK,response.getStatusCodeValue());
		assertEquals("JUNIOR, Paulo Pereira",response.getBody());
	}
}
