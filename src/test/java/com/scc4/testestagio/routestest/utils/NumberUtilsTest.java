package com.scc4.testestagio.routestest.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.scc4.testestagio.routestest.utils.NumberUtils;

@SpringBootTest
public class NumberUtilsTest {
	
	static List<Integer> listInteger;
	static List<Integer> listIntegerReverse;
	static List<Integer> listIntegerOdd;
	static List<Integer> listIntegerPair;
	
	@BeforeAll
	public static void createListForTests() {
		listInteger = new ArrayList<Integer>();
		listIntegerReverse = new ArrayList<Integer>();
		listIntegerOdd = new ArrayList<Integer>();
		listIntegerPair = new ArrayList<Integer>();
		
		listInteger.add(25);
		listInteger.add(35);
		listInteger.add(40);
		listInteger.add(21);
		
		listIntegerReverse.add(21);
		listIntegerReverse.add(40);
		listIntegerReverse.add(35);
		listIntegerReverse.add(25);
		
		listIntegerOdd.add(25);
		listIntegerOdd.add(35);
		listIntegerOdd.add(21);
		
		listIntegerPair.add(40);
	}
	
	@Test
	public void testReverseIntegerList() {
		List<Integer> listClone = new ArrayList<Integer>(listInteger); 
		List<Integer> reverseList = NumberUtils.reverseIntegerList(listInteger);
		
		assertIterableEquals(reverseList,listIntegerReverse);
		assertIterableEquals(listInteger,listClone);
	}
	
	@Test
	public void testFilterOdd() {
		List<Integer> listClone = new ArrayList<Integer>(listInteger); 
		List<Integer> oddList = NumberUtils.filterOdd(listInteger);
		
		assertIterableEquals(oddList,listIntegerOdd);
		assertIterableEquals(listInteger,listClone);
	}
	
	@Test
	public void testFilterPair() {
		List<Integer> listClone = new ArrayList<Integer>(listInteger); 
		List<Integer> pairsList = NumberUtils.filterpairs(listInteger);
		
		assertIterableEquals(pairsList,listIntegerPair);
		assertIterableEquals(listInteger,listClone);
	}
	
	@Test
	public void testIntegerListOfString() {
		List<Integer> list = NumberUtils.integerListOfString("25,35,40,21");
		
		assertIterableEquals(list,listInteger);
	}
	
	@Test
	public void testMessageArrayInteger() {
		String message = NumberUtils.messageArrayInteger(listInteger);
		
		assertEquals("{25,35,40,21}", message);
	}
	
	
}
