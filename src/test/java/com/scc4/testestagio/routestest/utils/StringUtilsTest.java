package com.scc4.testestagio.routestest.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class StringUtilsTest {
	
	
	@Test
	public void testFilterVowels() {
		String vowels = StringUtils.filterVowels("desafio");
		
		assertEquals(vowels,"eaio");
	}
	
	@Test
	public void testFilterVowelsUpperCase() {
		String vowels = StringUtils.filterVowels("DESAFIO");
		
		assertEquals(vowels,"EAIO");
	}
	
	
	@Test
	public void testFilterConsonants() {
		String consonants = StringUtils.filterConsonants("desafio");
		
		assertEquals(consonants,"dsf");
	}
	
	@Test
	public void testFilterConsonantsUpperCase() {
		String consonants = StringUtils.filterConsonants("DESAFIO");
		
		assertEquals(consonants,"DSF");
	}
	
	@Test
	public void nameForBibliographicName() {
		String bibliographicName = StringUtils.nameForBibliographicName("paulo pereira junior");
		
		assertEquals(bibliographicName,"JUNIOR, Paulo Pereira");
	}
	
}
