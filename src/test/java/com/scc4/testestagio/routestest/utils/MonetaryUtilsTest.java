package com.scc4.testestagio.routestest.utils;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.scc4.testestagio.routestest.utils.MonetaryUtils;

@SpringBootTest
public class MonetaryUtilsTest {

	@Test
	public void testWithdrawLessBankNotes() {
		int[] resultInput8 = MonetaryUtils.consultWithdrawLessBankNotes(8);
		int[] resultInput9 = MonetaryUtils.consultWithdrawLessBankNotes(9);
		int[] resultInput30 = MonetaryUtils.consultWithdrawLessBankNotes(30);
		
		assertArrayEquals(resultInput8, new int[] {1,1});
		assertArrayEquals(resultInput9, new int[] {0,3});
		assertArrayEquals(resultInput30, new int[] {6,0});
	}
	
	@Test
	public void testMessageQuantityBankNotes() {
		String messageOneBankNoteFive= MonetaryUtils.messageQuantityBankNotes(1, 5);
		String messageMoreBankNotesFive= MonetaryUtils.messageQuantityBankNotes(3, 5);
		
		String messageOneBankNoteThree= MonetaryUtils.messageQuantityBankNotes(1, 3);
		String messageMoreBankNotesThree= MonetaryUtils.messageQuantityBankNotes(3, 3);
		
		assert(messageOneBankNoteFive).equals(" 1 nota de R$5");
		assert(messageMoreBankNotesFive).equals(" 3 notas de R$5");
		
		assert(messageOneBankNoteThree).equals(" 1 nota de R$3");
		assert(messageMoreBankNotesThree).equals(" 3 notas de R$3");
	}
}
